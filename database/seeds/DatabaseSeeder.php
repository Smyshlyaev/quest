<?php

use App\Seeds\UserTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        factory(App\Models\Theme::class, 10)->create();
        factory(App\Models\Question::class, 1000)->create();
    }
}
