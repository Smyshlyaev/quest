<?php

namespace App\Seeds;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => config('auth.main.user.name'),
            'email' => config('auth.main.user.email'),
            'password' => Hash::make(config('auth.main.user.password')),
        ]);
    }
}
