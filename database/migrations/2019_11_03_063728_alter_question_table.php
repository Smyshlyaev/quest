<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->string('question')->nullable()->default(null)->change();
            $table->text('answer')->nullable()->default(null)->change();
            $table->integer('status')->nullable()->default(null)->change();
            $table->integer('theme')->nullable()->default(null)->change();
            $table->text('answer')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->string('question')->change();
            $table->text('answer')->change();
            $table->integer('status')->change();
            $table->integer('theme')->change();
            $table->text('answer')->nullable()->change();
        });
    }
}
