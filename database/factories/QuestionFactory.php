<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence,
        'answer' => $faker->sentence,
        'theme' => rand(1, 10),
        'status' => rand(1, 14),
        'is_oral' => rand(0, 1),
    ];
});
