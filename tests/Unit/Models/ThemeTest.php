<?php

namespace Tests\Unit\Models;

use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThemeTest extends TestCase
{
    /**
     * Тест добавления новой темы
     *
     * @return void
     */
    public function testThemaTests()
    {
        $faker = Factory::create();
        $response = $this->json('POST', '/themes', [
            'title' => $faker->text(190),
        ]);

        $response->assertStatus(302);
    }
}
