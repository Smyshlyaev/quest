<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeControllerTest extends TestCase
{
    /**
     * Проверка неавторизованного доступа к базовому контроллеру
     *
     * @return void
     */
    public function testAccessHome()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/home');

        $response->assertStatus(200);
    }
}
