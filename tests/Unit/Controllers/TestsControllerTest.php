<?php

namespace Tests\Unit\Controllers;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestsControllerTest extends TestCase
{
    /**
     * Проверка неавторизованного доступа к основным тестам
     *
     * @return void
     */
    public function testAccessMainTests()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/menu-tests/1');

        $response->assertStatus(200);
    }

    /**
     * Проверка неавторизованного доступа к второстепенным тестам
     *
     * @return void
     */
    public function testAccessSecondaryTests()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/menu-tests/2');

        $response->assertStatus(200);
    }
}
