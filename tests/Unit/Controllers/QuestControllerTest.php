<?php

namespace Tests\Unit;

use App\Http\Controllers\Admin\QuestsController;
use App\Http\Controllers\Api\V2\UserController;
use App\Models\User;
use ReflectionClass;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestControllerTest extends TestCase
{
    /**
     * @var ReflectionClass
     */
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->controller = new ReflectionClass(QuestsController::class);
    }

    /**
     * Проверяет, есть ли action index в контроллере вопросов
     */
    public function test_quest_controller_has_index_action()
    {
        $this->assertTrue($this->controller->hasMethod('index'));
    }

    /**
     * Проверка неавторизованного доступа к новым темам
     *
     * @return void
     */
    public function testAccessMenu()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/menu');

        $response->assertStatus(200);
    }
}
