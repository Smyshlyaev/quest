<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CounterControllerTest extends TestCase
{
    /**
     * Проверка неавторизованного доступа к основным счетчикам
     *
     * @return void
     */
    public function testAccessCounters()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/counters');

        $response->assertStatus(200);
    }
}
