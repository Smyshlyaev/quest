<?php

namespace Tests\Feature\Controllers;

use App\Http\Controllers\Admin\QuestsController;
use ReflectionClass;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestsControllerTest extends TestCase
{
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new ReflectionClass(QuestsController::class);
    }

    public function test_quests_controller_index_action_exists()
    {
        $this->assertTrue($this->controller->hasMethod('index'));
    }
}
