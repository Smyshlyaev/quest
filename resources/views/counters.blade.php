@extends('layouts.app')
@section('content')
    @if(isset($message))
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @endif
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Тема</th>
            <th scope="col">Счетчик</th>
            <th scope="col">Обнулить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($themes as $theme)
            <tr>
                <td>{{ $theme->id }}</td>
                <td>{{ $theme->title }}</td>
                <td>{{ $theme->counter }}</td>
                <td>
                    <form
                        method="POST"
                        action="{{route('counters.to.zero', ['id'=>$theme->id,])}}"
                    >
                        @csrf
                        <button type="submit" class="material-icons">
                            <i class="material-icons">clear</i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $themes->links() }}
@endsection
