@php($message = Session::get('message'))
@extends('layouts.app')
@section('content')

    <div class="card question-field">
        <h5 class="card-header">{{ $question->question }}</h5>
        <div class="card-body">
            <div class="form-group question-answer">
                <textarea class="form-control"rows="7">{{ $question->answer }}</textarea>
            </div>
            <p>ID: {{ $question->id }}</p>
            <p>Status: {{ $question->status }}</p>
            <p>Updated: {{ $question->updated_at }}</p>
            <p>Тема: {{ $question->themes->title }}</p>
            <p>Счетчик: {{ $question->themes->counter }}</p>
            <p>Общее количество: {{ $quantity }}</p>

            <button class="btn btn-primary show-answer">Подсказка</button>

            <form
                method="POST"
                action="{{route('positive', $answerParameters)}}">
                @csrf
                <button type="submit" class="btn btn-success">Правильный ответ</button>
            </form>

            <form
                method="POST"
                action="{{route('negative', $answerParameters)}}">
                <input type="hidden"
                       name="theme"
                       value="{{ $request->theme }}">
                @csrf
                <button type="submit" class="btn btn-danger">Ошибка</button>
            </form>
            <a href="{{route('quests.edit', $question)}}"
               type="button"
               class="btn btn-secondary"
            >
                Редактировать
            </a>
            <form
                action="{{ route('admin.destroy', $answerParameters)}}"
                method="post">
                @method('DELETE')
                @csrf
                <input type="hidden"
                       name="theme"
                       value="{{ $request->theme }}">
                <input class="btn btn-warning" type="submit" value="Delete" />
            </form>

{{--            <a href="{{ route('tests',['theme'=>$theme->id,'type'=>$type,]) }}" type="button" class="btn btn-secondary">--}}
{{--                {{ $type == 1 ? 'Устный тест' : 'Письменный тест' }} - {{ $theme->title }}({{ $theme->id }})--}}
{{--            </a>--}}

        </div>
    </div>

@endsection
