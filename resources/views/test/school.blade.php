@extends('layouts.app')
@section('content')
    <div class="card question-field">
        <div class="card-body">
            <h5>Тема: {{  $theme }}  </h5>
            <p>Вопрос: {{ $question->question }}  </p>
            <div class="form-group question-answer">
                <textarea class="form-control"rows="7">{{ $question->answer }}</textarea>
            </div>
            <p>ID: {{  $question->id  }} </p>
            <p>Status: {{ $question->status }}  </p>
            <p>Updated: {{ $question->updated_at }}  </p>
            <p>Общее количество: {{ $count }}</p>

            <button class="btn btn-primary show-answer">Подсказка</button>
            <form
                method="POST"
                action="{{route('school.positive', [$question->id])}}">
                @csrf
                <input type="hidden" name="class" value="{{ $class }}">
                <button type="submit" class="btn btn-success">Правильный ответ</button>
            </form>
            <form
                method="POST"
                action="{{route('school.negative', [$question->id])}}">
                @csrf
                <input type="hidden" name="class" value="{{ $class }}">
                <button type="submit" class="btn btn-danger">Ошибка</button>
            </form>

            <a href="{{route('quests.edit', $question)}}"
               type="button"
               class="btn btn-secondary"
            >
                Редактировать
            </a>
            <form
                action="{{ route('school.delete', $question)}}"
                method="post">
                @method('DELETE')
                @csrf
                <input type="hidden" name="class" value="{{ $class }}">
                <input class="btn btn-warning" type="submit" value="Delete" />
            </form>

        </div>
    </div>
@endsection
