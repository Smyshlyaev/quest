@php($message = Session::get('message'))
@extends('layouts.app')
@section('content')
    @if(isset($message))
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @endif
    <a href="{{ route('themes.create') }}" class="btn btn-primary float-right add-note">Добавить</a>
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Название темы</th>
            <th scope="col">Статус</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($themes as $theme)
        <tr>
            <td>{{ $theme->id }}</td>
            <td>{{ $theme->title }}</td>
            <td>{{ $theme->enable ? 'Включен' : '' }}</td>
            <td>
                <a href="{{route('themes.edit', $theme)}}">
                    <i class="material-icons">edit</i>
                </a>
            </td>
            <td>
                <form
                    method="POST"
                    action="{{route('themes.destroy', $theme)}}"
                >
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="material-icons">
                        <i class="material-icons">clear</i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    {{ $themes->links() }}
@endsection
