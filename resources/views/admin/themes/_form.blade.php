<form
    method="POST"
    action="{{ isset($theme) ? route('themes.update', $theme) : route('themes.store') }}"
    enctype="multipart/form-data"
>
    @csrf
    @if (isset($theme))
        @method('PATCH')
    @endif
    <div class="form-group">
        <label for="theme">Новая тема</label>
        <input  type="text"
                class="form-control"
                id="title"
                name="title"
                value="{{ isset($theme) ? $theme->title : null }}"
        >
        <small class="form-text text-muted">Название темы</small>
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-enabled" id="enabled" name="enabled"
               @if(isset($theme) && $theme->enable)
               checked
            @endif
        >
        <label class="form-enabled" for="enabled">Включить</label>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>
