@extends('layouts.app')
@section('content')
    <form action="{{ route('common.index') }}" method="GET">
        @foreach($themes as $theme)
        <div class="checkbox">
            <label>
                <input type="checkbox"
                       name="theme"
                       value="{{ $theme->id }}"
                > {{ $theme->title }}
            </label>
        </div>
        @endforeach
        <div class="checkbox">
            <label>
                <input type="radio"
                       name="type"
                       value="1"
                > Устные тесты
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="radio"
                       name="type"
                       value="2"
                > Письменные тесты
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="radio"
                       name="type"
                       value="3"
                > Общие тесты тесты
            </label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
