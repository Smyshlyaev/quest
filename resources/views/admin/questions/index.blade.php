@php($message = Session::get('message'))
@extends('layouts.app')
@section('content')
    <h2>Тема: {{ $themeInfo->title }}</h2>
    @if(isset($message))
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @endif
    <form method="GET" action="{{ route('quests.search', ['theme' => $theme,]) }}">
        <div class="form-group">
            <label for="exampleInputEmail1">Поиск по содержимому</label>
            <input type="search" class="form-control" id="searchInput" name="search">
            <input type="hidden" name="theme" value="{{ $theme }}">
        </div>
        <button type="submit" class="btn btn-primary">Найти</button>
    <a href="{{ route('quests.create', ['theme' => $theme,]) }}" class="btn btn-primary float-right add-note">Добавить</a>
    </form>
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Вопрос</th>
            <th scope="col">Тип ответа</th>
            <th scope="col">Статус</th>
            <th scope="col">Обновлено</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($questions as $question)
            <tr>
                <td>{{ $question->id }}</td>
                <td>{{ $question->question }}</td>
                <td>{{ $question->is_oral ? 'Устный' : '' }}</td>
                <td>{{ $question->status }}</td>
                <td>{{ $question->updated_at }}</td>
                <td>
                    <a href="{{route('quests.edit', $question)}}">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
                <td>
                    <form
                        method="POST"
                        action="{{route('quests.destroy', $question)}}"
                    >
                        <input type="hidden"
                               name="theme"
                               value="{{ $theme }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="material-icons">
                            <i class="material-icons">clear</i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $questions->appends(['theme' => $theme])->links() }}
@endsection

