<form
    method="POST"
    action="{{ isset($question) ? route('quests.update', $question) : route('quests.store') }}"
    enctype="multipart/form-data"
>
    @csrf
    @if (isset($question))
        @method('PATCH')
    @endif

    <div class="form-group">
        <label for="theme">Новый вопрос</label>
        <input  type="text"
                class="form-control"
                id="title"
                name="question"
                value="{{ isset($question) ? $question->question : null }}"
                required
        >
        <br>
        <select name="theme">
            @foreach ($allThemes as $theme)
                <li><option
                    @if((isset($request->theme) ? $request->theme : $question->theme)===$theme->id)
                        selected
                    @endif
                    value="{{ $theme->id }}"
                    >{{ $theme->title }}</option></li>
            @endforeach
        </select>
        <br>
        <small class="form-text text-muted">Текст вопроса</small>
    </div>

    <div class="form-group">
<textarea class="form-control"
          name="answer"
          id="exampleFormControlTextarea1"
          rows="6"
          required
>{{ isset($question) ? $question->answer : null }}</textarea>
        <small class="form-text text-muted">Ответ</small>
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-enabled" id="is_oral" name="is_oral"
               @if(isset($question) && $question->is_oral)
               checked
            @endif
        >
        <label class="form-enabled" for="enabled">Устный ответ</label>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>
