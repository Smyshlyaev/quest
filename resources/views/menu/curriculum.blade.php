@extends('layouts.app')
@section('content')
    <div class="btn-group-vertical">
        <h3>Формат обучения:</h3>
        <form method="post" action="{{ route('curriculum.store') }}">
            @csrf
            <div class="form-group">
                <div class="radio">
                    <label>
                        <input type="radio" name="format" id="optionsRadios1" value="written"
                               @if ($format === 'written')
                               checked
                            @endif
                        >
                        Письменная программа
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="format" id="optionsRadios2" value="oral"
                               @if ($format === 'oral')
                               checked
                            @endif
                        >
                        Устная программа
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="format" id="optionsRadios2" value="complex"
                               @if ($format === 'complex')
                               checked
                            @endif
                        >
                        Комплексная программа
                    </label>
                </div>
                <h3>Темы:</h3>
                @foreach($themes as $theme)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="theme[]" value="{{ $theme->id }}"
                                   @if ($theme->in_school === true)
                                   checked
                                @endif
                            >
                            {{ $theme->title }}
                            @if ($theme->in_school === true)
                                checked
                            @endif
                        </label>
                    </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
