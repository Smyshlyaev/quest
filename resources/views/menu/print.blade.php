@extends('layouts.app')
@section('content')
    <div class="btn-group-vertical">
        <h3>Выбор тестов на печать</h3>
        @foreach($themes as $theme)
            @if($theme->enable)
                <a href="{{ route('print.page',['theme'=>$theme->id,'type'=>$type,]) }}" type="button" class="btn btn-secondary">
                    {{ $type == 1 ? 'Устный тест' : 'Письменный тест' }} - {{ $theme->title }}({{ $theme->quantity }})
                </a>
            @endif
        @endforeach

    </div>
@endsection
