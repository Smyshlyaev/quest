@extends('layouts.app')
@section('content')
    <div class="btn-group-vertical">
        @foreach($themes as $theme)
            @if($theme->enable)
                <a href="{{ route('quests.index',['theme'=>$theme->id,]) }}" type="button" class="btn btn-secondary">{{ $theme->title }}({{ $theme->id }})</a>
            @endif
        @endforeach
    </div>
@endsection

