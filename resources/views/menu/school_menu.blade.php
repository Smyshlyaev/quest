@extends('layouts.app')
@section('content')
    <div class="btn-group-vertical">
        <h3>Выбор класса:</h3>
        <form method="get" action="{{ route('school.question') }}">
            @csrf
            @for($i = 1; $i <= 14; $i++)
            <div class="form-group">
                <div class="radio">
                    <label>
                        <input type="radio" name="class" id="optionsRadios1" value="{{ $i }}">
                        {{ $i }} класс [{{ $counts[$i]}}]
                    </label>
                </div>
            </div>
            @endfor
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
