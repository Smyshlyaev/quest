<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <script src="/js/jquery.js"></script>
</head>
    <body>
        @yield('content')
        <script src="js/textarea.js"></script>
        <script src="/js/print.js"></script>
    </body>
</html>
