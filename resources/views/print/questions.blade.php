@extends('layouts.print')
@section('content')
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>--}}
<div style="margin-bottom:350px;">
@foreach($questions as $question)
    <p>{{$question->status}}.{{$question->question}}</p>
@endforeach
</div>
@foreach($questions as $question)
    <p style="font-weight:bold">{{$question->status}}.{{$question->question}}</p>
    <textarea oninput="textAreaAdjust(this)" id="textarea" style="width:100%;border-style:none;display: block;" data="elastic">
        {{$question->answer}}
    </textarea>
@endforeach


@endsection
