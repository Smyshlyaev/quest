@extends('layouts.app')
@section('content')
    <form
        method="GET"
        action="{{ route('large.interview.quest') }}"
        enctype="multipart/form-data"
    >
        @csrf
        @foreach($themes as $theme)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="{{ $theme->id }}"> {{ $theme->title }}
            </label>
        </div>
        @endforeach
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
@endsection
