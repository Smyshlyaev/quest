<li class="nav-item">
    <a class="nav-link"  href="{{ route('themes.index') }}">
        <span class="nav-link-text">Тема</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('menu') }}">
        <span class="nav-link-text">Новые вопросы</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('curriculum.menu') }}">
        <span class="nav-link-text">Программа школы</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('school.menu') }}">
        <span class="nav-link-text">Общее тестирование</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('menu.tests',['type'=>1]) }}">
        <span class="nav-link-text">Устные тесты</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('menu.tests',['type'=>2]) }}">
        <span class="nav-link-text">Письменные тесты</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('counters') }}">
        <span class="nav-link-text">Счетчики</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link"  href="{{ route('print.menu') }}">
        <span class="nav-link-text">Распечатка письменных тестов</span>
    </a>
</li>



