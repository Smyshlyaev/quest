<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'Admin\HomeController@index')->name('home');
    Route::get('/menu', 'Admin\Menu\QuestsController@index')->name('menu')->middleware('auth');
    Route::get('/menu-tests/{type}', 'Admin\Menu\TestsController@index')->name('menu.tests');
    Route::resource('/themes', 'Admin\ThemesController');
    Route::resource('/quests', 'Admin\QuestsController')->middleware('auth');
    Route::get('/common_menu', 'Admin\CommonController@menu')->name('common.menu');
    Route::get('/tests', 'Admin\TestController@index')->name('tests')->middleware('auth');
    Route::delete('/tests/delete/{test}', 'Admin\TestController@delete')->name('admin.destroy');
    Route::post('/positive/{theme}/{id}/{is_oral}', 'Admin\TestController@positive')->name('positive');
    Route::post('/negative/{theme}/{id}/{is_oral}', 'Admin\TestController@negative')->name('negative');
    Route::get('/counters', 'Admin\CountersController@index')->name('counters');
    Route::post('/counters/to_zero/{id}', 'Admin\CountersController@zero')->name('counters.to.zero');
    Route::get('/search/', 'Admin\QuestsController@search')->name('quests.search');
    Route::get('/common_index', 'Admin\CommonController@index')->name('common.index');
    Route::get('/print-menu', 'Admin\Menu\PrintController@menu')->name('print.menu');

    Route::get('/curriculum', 'Admin\Menu\CurriculumController@main')->name('curriculum.menu');
    Route::post('/curriculum-store', 'Admin\Menu\CurriculumController@store')->name('curriculum.store');
    Route::get('/school-menu', 'Admin\SchoolController@menu')->name('school.menu');
    Route::get('/school-question', 'Admin\SchoolController@question')->name('school.question');
    Route::post('/school-positive/{question}', 'Admin\SchoolController@positive')->name('school.positive');
    Route::post('/school-negative/{question}', 'Admin\SchoolController@negative')->name('school.negative');
    Route::delete('/school-delete/{question}', 'Admin\SchoolController@delete')->name('school.delete');

    Route::get('print-page', 'Admin\PrintPageController@index')->name('print.page');
    Route::get('/large-intervew', 'Admin\LargeInterviewController@menu')->name('large.interview.menu');
    Route::get('/large-intervew-quest', 'Admin\LargeInterviewController@quest')->name('large.interview.quest');
});
Route::get('/mysql','Redirects\MysqlController@index');

