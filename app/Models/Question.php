<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function themes()
    {
        return $this->belongsTo('App\Models\Theme', 'theme', 'id');
    }
}
