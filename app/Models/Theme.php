<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Theme extends Model
{
    /**
     * Подключен к этой таблице
     *
     * @var string
     */
    protected $table = 'themes';

    protected $fillable = ['in_school'];

    /**
     * Выдает все темы в виде массива
     *
     * @param Request $request
     * @return array
     */
    public function getAllThemesByArray()
    {
        $themes = $this->all();

        $result = [];
        foreach($themes as $theme){
            $result[$theme->id] = $theme->title;
        }

        return $result;
    }

}
