<?php

namespace App\Helpers;

use App\Http\Services\QuestionService;
use App\Models\Theme;

class ThemeHelper
{
    public static function list($type)
    {
        $themes = Theme::get();
        $questionService = new QuestionService();

        foreach($themes as &$theme){
            $theme->quantity = $questionService->getQuantityQuestions($theme->id, $type);
        }

        return $themes->sortByDesc('quantity');
    }
}
