<?php

namespace App\Http\Services;


use App\Repositories\QuestionRepository;
use Illuminate\Http\Request;

class SchoolService
{
    private $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    public function getQuestion(Request $request)
    {
        return $this->questionRepository->getQuestionByClass((int)$request->class);
    }

    public function setQuestionPositive(int $id)
    {
        $question = $this->questionRepository->getQuestionById($id);
        $question->status++;
        $this->questionRepository->saveQuestion($question);
    }

    public function setQuestionNegative(int $id)
    {
        $question = $this->questionRepository->getQuestionById($id);
        if ($question->status > 1) {
            $question->status = 1;
        }
        $this->questionRepository->saveQuestion($question);
    }

    public function deleteQuestion(int $id)
    {
        $question = $this->questionRepository->getQuestionById($id);
        $this->questionRepository->deleteQuestion($question);
    }
}
