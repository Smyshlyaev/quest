<?php

namespace App\Http\Services;

use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QuestionService
{
    /**
     * Тема текущего вопроса
     *
     * @var integer
     */
    private $theme;

    /**
     * Временные задержки перед вопросами
     *
     * @var array
     */
    private $delays = [
        '-1 seconds',
        '-1 minutes',
        '-15 minutes',
        '-1 hours',
        '-2 hours',
        '-3 hours',
        '-1 days',
        '-2 days',
        '-3 days',
        '-1 weeks',
        '-2 weeks',
        '-3 weeks',
        '-1 months',
        '-2 months',
        '-3 months',
    ];

    /**
     * Получаем вопрос для теста
     *
     * @param Request $request
     * @return bool
     */
    public function getQuestion(Request $request)
    {
        $this->theme = $request->theme;
        $theme = [$request->theme];
        $type = $request->type;

            $question = $this->getQuestionCriteria($theme, $type)
            ->orderBy('status', 'ASC')
            ->first();

        return $question == null ? false : $question;
    }

    /**
     * @param Request $request
     * @return false
     */
    public function getManyQuestion(Request $request)
    {
        $this->theme = $request->theme;
        $theme = [$request->theme];
        $type = $request->type;

        $question = $this->getQuestionCriteria($theme, $type)
            ->orderBy('status', 'ASC')
            ->limit(config('print.limit_print_questions'))
            ->get();

        return $question == null ? false : $question;
    }

    /**
     * Получить количество вопросов
     *
     * @param $theme
     * @param $type
     * @return mixed
     */
    public function getQuantityQuestions($theme, $type)
    {
        $quantity = $this->getQuestionCriteria([$theme], $type)
            ->count();

        return $quantity;
    }

    /**
     * Возвращает критерии теста
     *
     * @param $theme
     * @param $type
     * @param bool $isLargeInterview
     * @return Question
     */
    private function getQuestionCriteria($theme, $type, bool $isLargeInterview = false)
    {
        $question = new Question();
        $question = $question->with('themes');

        for ($status = 0; $status <= 14; $status++) {
            $question = $question->orWhereIn('theme', $theme);
            if (!$isLargeInterview) {
                $question = $question->where('is_oral', $type == 2 ? 0 : 1);
            }
            $question = $question->where(
                    'updated_at',
                    '<',
                    date("Y-m-d H:i:s", strtotime($this->delays[$status]))
                )
                ->where('status', $status);
        }

        return $question;
    }
}
