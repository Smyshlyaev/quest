<?php

namespace App\Http\Controllers\Admin;

use App\Acme\Visual\Enums\VisualParameters;
use App\Http\Requests\QuestRequest;
use App\Models\Question;
use App\Models\Theme;
use App\Repositories\QuestionRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\QuestionRepository;

/**
 * Class QuestsController
 * @package App\Http\Controllers\Admin
 */
class QuestsController extends BaseController
{
    /**
     * Это экземпляр класса Question
     *
     * @var Question
     */
    private $question;

    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * Это экземпляр класса Theme
     *
     * @var Theme
     */
    private $theme;

    /**
     * QuestsController constructor.
     * @param Question $question
     * @param Theme $theme
     */
    public function __construct(Question $question,
                                Theme $theme,
                                QuestionRepository $questionRepository)
    {
        $this->question = $question;
        $this->theme = $theme;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Выводим список вопросов
     *
     * @return \Illuminate\Http\Response
     */
    public function index(QuestRequest $request)
    {
        $themeInfo = Theme::find($request->theme);
        $questions = $this->questionRepository
            ->getQuestionsByTheme($request->theme)
            ->paginate(VisualParameters::PAGINATE);
        $theme = $request->theme;

        return view(
            'admin.questions.index',
            compact('questions', 'request', 'theme', 'themeInfo')
        );
    }

    /**
     * Контроллер с результатами поиска
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $themeInfo = Theme::find($request->theme);
        $questions = $this->questionRepository->search($request)
            ->paginate(VisualParameters::PAGINATE);
        $theme = $request->theme;

        return view('admin.questions.index', compact('questions', 'request', 'theme', 'themeInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $themes = $this->theme->getAllThemesByArray();
        $allThemes = Theme::where('enable', '=', 1)->get();

        return view('admin.questions.create', compact('themes', 'request', 'allThemes'));
    }

    public function store(Request $request)
    {
        $theme = $this->theme
            ->find($request->theme);
        $theme->counter++;
        $theme->update();

        $question = $this->question;
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->status = 1;
        $question->theme = $request->theme;
        $question->is_oral = isset($request->is_oral) ? true : false;
        $message = $question->save()
            ? 'Новый вопрос ' . substr($request->question, 0, 100) . ' успешно добавлен.Счетчик:' . $theme->counter
            : 'Добавить новый вопрос не удалось';

        return redirect(route('quests.index', ['theme' => $request->theme]))
            ->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = $this->question->find($id);
        $themeInfo = Theme::find($question->theme);
        $allThemes = Theme::where('enable', '=', 1)->get();

        return view('admin.questions.edit', compact(
            'question',
            'themeInfo',
            'allThemes'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = $this->question->find($id);
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->status = 1;
        $question->theme = $request->theme;
        $question->is_oral = isset($request->is_oral) ? true : false;
        $message = $question->update()
            ? 'Новый вопрос ' . substr($request->question, 0, 100) . ' успешно обновлен'
            : 'Обновиться не удалось';


        return redirect(route('quests.index', ['theme' => $request->theme]))
            ->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        $question = $this->question->find($id);

        $message = $question->delete()
            ? 'Вопрос ' . substr($question->question, 0, 100) . ' был удален'
            : 'Удалить не удалось.';
        $questions = $this->question->where('theme', $request->theme)
            ->orderBy('id', 'desc')
            ->paginate(VisualParameters::PAGINATE);
        return redirect(route('quests.index', ['theme' => $request->theme, 'questions' => $questions,]))
            ->with('message', $message);
    }
}
