<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Helpers\ThemeHelper;
use App\Http\Controllers\Admin\BaseController;

/**
 * Class TestsController
 * @package App\Http\Controllers\Admin\Menu
 */
class TestsController extends BaseController
{
    /**
     *  Меню для выбора темы вопросов
     * @param $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($type)
    {
        return view('menu.tests', [
            'themes' => ThemeHelper::list($type),
            'type'   => $type,
        ]);
    }
}
