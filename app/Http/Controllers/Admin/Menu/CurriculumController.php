<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Repositories\ThemeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurriculumController extends Controller
{
    private $themeRepository;

    public function __construct(ThemeRepository $themeRepository)
    {
        $this->themeRepository = $themeRepository;
    }

    public function main()
    {
        return view('menu.curriculum', [
            'themes' => $this->themeRepository->getAllEnabledThemes(),
            'format' => $this->themeRepository->getFormatToLearn(),
        ]);
    }

    public function store(Request $request)
    {
        $this->themeRepository->storeFormatToLearn($request);
        $this->themeRepository->storeThemeInSchool($request);

        return view('menu.curriculum_store');
    }
}
