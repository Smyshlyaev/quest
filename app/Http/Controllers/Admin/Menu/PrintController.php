<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Helpers\ThemeHelper;
use App\Http\Controllers\Controller;

class PrintController extends Controller
{
    private const WRITTEN_TESTS = 2;

    public function menu()
    {
        return view('menu.print', [
            'themes' => ThemeHelper::list(self::WRITTEN_TESTS),
            'type'   => self::WRITTEN_TESTS,
        ]);
    }
}
