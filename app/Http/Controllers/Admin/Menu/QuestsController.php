<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Models\Theme;
use App\Http\Controllers\Admin\BaseController;

/**
 * Class QuestsController
 * @package App\Http\Controllers\Admin\Menu
 */
class QuestsController extends BaseController
{
    /**
     * Это экземпляр Theme
     *
     * @var Theme
     */
    private $theme;

    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    /**
     * Меню для выбора темы вопросов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('menu.quests', [
            'themes' => $this->theme->get(),
        ]);
    }
}
