<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PrintRequest;
use App\Http\Services\QuestionService;
use App\Repositories\QuestionRepository;

class PrintPageController extends Controller
{
    /**
     * @var QuestionService
     */
    private $questionService;

    /**
     * PrintPageController constructor.
     * @param QuestionService $questionService
     */
    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    /**
     * @param PrintRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(PrintRequest $request)
    {
        $questions = $this->questionService
            ->getManyQuestion($request);

        return view('print.questions', compact('questions'));
    }
}
