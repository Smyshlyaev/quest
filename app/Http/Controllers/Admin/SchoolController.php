<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\SchoolService;
use App\Repositories\QuestionRepository;
use App\Repositories\ThemeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolController extends Controller
{
    private $questionRepository;

    private $schoolService;

    private $themeRepository;

    public function __construct(
        QuestionRepository $questionRepository,
        SchoolService $schoolService,
        ThemeRepository $themeRepository
    )
    {
        $this->questionRepository = $questionRepository;
        $this->schoolService = $schoolService;
        $this->themeRepository = $themeRepository;
    }

    public function menu()
    {
        $counts = $this->getClassesCounts();
        return view('menu.school_menu', ['counts' => $counts]);
    }

    private function getClassesCounts()
    {
        $counts = [];
        for ($class = 1; $class <= 14; $class++) {
            $counts[$class] = $this->questionRepository->count($class);
        }

        return $counts;
    }

    public function question(Request $request)
    {
        $question = $this->schoolService->getQuestion($request);
        if (!$question) {

            return view('test.empty');
        }

        $theme = $this->themeRepository->getById($question->theme);
        $count = $this->questionRepository->count($request->class);

        return view('test.school',
                    [
                        'question' => $question,
                        'count' => $count,
                        'class' => $request->class,
                        'theme' => $theme->title,
                    ]
        );
    }

    public function positive($id, Request $request)
    {
        $this->schoolService->setQuestionPositive((int)$id);

        return redirect()->route('school.question', $request);
    }

    public function negative($id, Request $request)
    {
        $this->schoolService->setQuestionNegative((int)$id);

        return redirect()->route('school.question', $request);
    }

    public function delete($id, Request $request)
    {
        $this->schoolService->deleteQuestion((int)$id);

        return redirect()->route('school.question', $request);
    }
}
