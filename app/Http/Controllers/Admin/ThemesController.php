<?php

namespace App\Http\Controllers\Admin;

use App\Models\Theme;
use Illuminate\Http\Request;

/**
 * Class ThemesController
 * @package App\Http\Controllers\Admin
 */
class ThemesController extends BaseController
{
    /**
     * Размер пагинации
     *
     * @var int
     */
    private $paginate = 25;

    /**
     * Это экземпляр класса Theme
     *
     * @var Theme
     */
    private $theme;

    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    /**
     * Общий список новых тем
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themes = $this->theme
            ->paginate($this->paginate);

        return view('admin.themes.index', compact('themes'));
    }

    /**
     * Форма создания новой темы
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.themes.create');
    }

    /**
     * Сохранений новой темы
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $theme = $this->theme;
        $theme->title = $request->title;
        $theme->enable = (isset($request->enabled)) ? true : false;
        $theme->save();

        return redirect(route('themes.index'))
            ->with('message', 'Новая тема ' . $theme->title . ' успешно добавленa');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Форма редактирования существующей темы
     *
     * @param Theme $theme
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Theme $theme, Request $request)
    {
        return view('admin.themes.edit', compact('theme'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Theme $theme, Request $request)
    {
        $theme->title = $request->title;
        $theme->enable = (isset($request->enabled)) ? true : false;
        $theme->save();

        return redirect(route('themes.index'))
            ->with('message', 'Тема ' . $theme->title . ' успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theme $theme)
    {
        $theme->delete();

        return redirect(route('themes.index'))
            ->with('message', 'Тема ' . $theme->title . ' удалена');
    }
}
