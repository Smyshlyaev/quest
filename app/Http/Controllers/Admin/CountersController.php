<?php

namespace App\Http\Controllers\Admin;

use App\Acme\Visual\Enums\VisualParameters;
use App\Models\Theme;
use App\Repositories\ThemeRepository;

/**
 * Class CountersController
 * @package App\Http\Controllers\Admin
 */
class CountersController extends BaseController
{
    /**
     * @var Theme
     */
    public $theme;

    /**
     * @var ThemeRepository
     */
    private $themeRepository;

    /**
     * CountersController constructor.
     * @param Theme $theme
     * @param ThemeRepository $themeRepository
     */
    public function __construct(Theme $theme, ThemeRepository $themeRepository)
    {
        $this->theme = $theme;
        $this->themeRepository = $themeRepository;
    }

    /**
     * Базовая страница счетчиков
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $themes = $this->themeRepository->getAllEnabledThemesWithPaginate();

        return view('counters', compact('themes'));
    }

    /**
     * Обнуление счетчиков
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function zero(string $id)
    {
        $theme = $this->themeRepository->getById($id);
        $theme->counter = 0;
        $theme->update();

        return redirect(route('counters'));
    }
}
