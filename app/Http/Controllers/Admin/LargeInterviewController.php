<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LargeInterviewRequest;
use App\Models\Theme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LargeInterviewController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function menu()
    {
        $themes = Theme::where(['enable' => true])->get();

        return view('large_interview.menu', ['themes' => $themes]);
    }

    public function quest(LargeInterviewRequest $request)
    {
        $x = $request->keys();

        return view('home');
    }
}
