<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ThemeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CommonController
 * @package App\Http\Controllers\Admin
 */
class CommonController extends Controller
{
    /**
     * @var ThemeRepository
     */
    private $themeRepository;

    /**
     * CommonController constructor.
     * @param ThemeRepository $themeRepository
     */
    public function __construct(ThemeRepository $themeRepository)
    {
        $this->themeRepository = $themeRepository;
    }

    /**
     * Главное меню общих вопросов
     *
     * @return \Illuminate\View\View
     */
    public function menu()
    {
        $themes = $this->themeRepository->getAll();

        return view('admin.common_menu.index', compact('themes'));
    }

    /**
     * Еще неготовый раздел
     */
    public function index(Request $request)
    {
        dd($request);
    }
}
