<?php

namespace App\Http\Controllers\Admin;

use App\Models\Question;
use App\Models\Theme;
use Illuminate\Http\Request;
use App\Http\Services\QuestionService;

/**
 * Class TestController
 * @package App\Http\Controllers\Admin
 */
class TestController extends BaseController
{
    /**
     * Это экземпляр класса QuestionService
     *
     * @var QuestionService
     */
    private $questionService;

    /**
     *  Это экземпляр класса Question
     *
     * @var Question
     */
    private $question;

    /**
     * Это экземпляр класса Theme
     *
     * @var Theme
     */
    private $theme;

    public function __construct(QuestionService $questionService, Question $question, Theme $theme)
    {
        $this->questionService = $questionService;
        $this->question = $question;
        $this->theme = $theme;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $question = $this->questionService->getQuestion($request);
        $theme = $request->theme;
        $type = $request->type;
        $quantity = $this->questionService->getQuantityQuestions($theme, $type);
        if ($question) {
            $answerParameters = [
                'theme' => $request->theme,
                'id' => $question->id,
                'is_oral' => (int)$question->is_oral,
            ];

            return view('test.question',
                compact('question', 'request', 'answerParameters', 'quantity')
            );
        }

        return view('test.empty');
    }

    /**
     * Правильный ответ
     *
     * @param $theme
     * @param $id
     * @param $is_oral
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function positive($theme, $id, $is_oral)
    {
        $question = $this->question
            ->find($id);
        $status = $question->status;
        $question->status = $status + 1;
        $question->update();

        $currentTheme = $this->theme->find($theme);
        $currentTheme->counter++;
        $currentTheme->update();

        $type = $is_oral == 1 ? 1 : 2;

        return redirect(route('tests') . '?theme=' . $theme . '&type=' . $type);
    }

    /**
     * Неправильный ответ
     *
     * @param $theme
     * @param $id
     * @param $is_oral
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function negative($theme, $id, $is_oral)
    {
        $question = $this->question->find($id);
        $question->status = 1;
        $question->update();

        $currentTheme = $this->theme->find($theme);
        $currentTheme->counter++;
        $currentTheme->update();

        $type = $is_oral == 1 ? 1 : 2;

        return redirect(route('tests') . '?theme=' . $theme . '&type=' . $type);
    }

    public function delete(Request $request)
    {
        $question = $this->question
            ->find($request->id)
            ->delete();
        $type = $request->is_oral == 1 ? 1 : 2;

        return redirect(route('tests') . '?theme=' . $request->theme . '&type=' . $type);
    }
}
