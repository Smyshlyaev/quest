<?php

namespace App\Repositories;

use App\Models\Question;
use Illuminate\Http\Request;

interface QuestionRepositoryInterface
{
    /**
     * QuestionRepository constructor.
     * @param Question $question
     */
    public function __construct(Question $question);

    /**
     * Возвращает вопросы заданной темы
     *
     * @param $theme
     * @return mixed
     */
    public function getQuestionsByTheme($theme);

    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request);
}
