<?php

namespace App\Repositories;

use App\Acme\Visual\Enums\VisualParameters;
use App\Models\Question;
use App\Models\Variable;
use Illuminate\Http\Request;

class QuestionRepository
{
    /**
     * @var Question
     */
    private $question;

    private $themeRepository;

    /**
     * QuestionRepository constructor.
     * @param Question $question
     */
    public function __construct(Question $question, ThemeRepository $themeRepository)
    {
        $this->question = $question;
        $this->themeRepository = $themeRepository;
    }

    /**
     * Возвращает вопросы заданной темы
     *
     * @param $theme
     * @return mixed
     */
    public function getQuestionsByTheme($theme)
    {
        $questions = $this->question
            ->where('theme', $theme)
            ->orderBy('id', 'desc');

        return $questions;
    }

    /**
     * Ищет вопросы
     *
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $questions = Question::where('theme', $request->theme)
            ->where('question', 'like', '%' . $request->search . '%')
            ->orderBy('id', 'desc');

        return $questions;
    }

    public function getQuestionByClass(int $class)
    {
        $question = $this->getCriteria($class)
            ->orderBy('updated_at', 'asc')
            ->first();
        if (is_null($question)) {

            return false;
        }

        return $question;
    }

    public function count(int $class)
    {
        return $this->getCriteria($class)->count();
    }

    /**
     * @param int $class
     * @return mixed
     */
    private function getCriteria(int $class)
    {
        $timeout = [
            1 => '-1 minute',
            2 => '-3 minutes',
            3 => '-15 minutes',
            4 => '-30 minutes',
            5 => '-1 hour',
            6 => '-3 hours',
            7 => '-6 hours',
            8 => '-12 hours',
            9 => '-1 day',
            10 => '-3 days',
            11 => '-1 week',
            12 => '-2 weeks',
            13 => '-1 month',
            14 => '-3 months',
        ];
        $themes = $this->themeRepository->getActivePermittedThemes()->pluck('id');
        $question = Question::where('status', '=', $class)
            ->whereIn('theme', $themes)
            ->where(
                'updated_at',
                '<',
                date("Y-m-d H:i:s", strtotime($timeout[$class]))
            );
        $variable = Variable::where('key', 'like', 'format_to_learn')->first();
        switch ($variable->value) {
            case 'oral':
                $question = $question->where('is_oral', '=', true);
                break;
            case 'written':
                $question = $question->where('is_oral', '=', false);
                break;
        }

        return $question;
    }

    public function getQuestionById(int $id)
    {
        return Question::where('id', '=', $id)->first();
    }

    public function saveQuestion($question)
    {
        $question->save();
    }

    public function deleteQuestion($question)
    {
        $question->delete();
    }
}
