<?php

namespace App\Repositories;

use App\Acme\Visual\Enums\VisualParameters;
use App\Models\Theme;
use App\Models\Variable;
use Illuminate\Support\Facades\DB;

class ThemeRepository
{
    /**
     * @var Theme
     */
    private $theme;

    /**
     * @var Variable
     */
    private $variable;

    /**
     * ThemeRepository constructor.
     * @param Theme $theme
     * @return void
     */
    public function __construct(Theme $theme, Variable $variable)
    {
        $this->theme = $theme;
        $this->variable = $variable;
    }

    /**
     * Возвращает все темы
     *
     * @return Theme[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->theme->all();
    }

    /**
     * Возвращает все разрешенные темы с пагинацией
     *
     * @return mixed
     */
    public function getAllEnabledThemesWithPaginate()
    {
        return $this->theme
            ->where('enable', 1)
            ->paginate(VisualParameters::PAGINATE);
    }

    /**
     * Возвращает все разрешенные темы без пагинации
     *
     * @return mixed
     */
    public function getAllEnabledThemes()
    {
        return $this->theme->where('enable', 1)->get();
    }

    /**
     * Возвращает тему по ID
     *
     * @param string $id
     * @return mixed
     */
    public function getById(string $id)
    {
        return $this->theme->find($id);
    }

    /**
     * @return mixed
     */
    public function getFormatToLearn()
    {
        $formatToLearn = $this->variable->where('key', 'like', 'format_to_learn');
        if ($formatToLearn->count() === 0) {
            $variable = new Variable();
            $variable->key = 'format_to_learn';
            $variable->value = 'complex';
            $variable->save();

            return 'complex';
        }

        return $formatToLearn->first()->value;
    }

    public function storeFormatToLearn($request)
    {
        $variable = Variable::where('key', 'like', '%format_to_learn%');
        $variable->update(
            [
                'value' => $request->format,
            ]
        );
    }

    public function storeThemeInSchool($request)
    {
        $ids = $this->theme->get()->pluck('id');
        foreach($ids as $id) {
            $theme = Theme::find($id);
            $theme->in_school = in_array($id, $request->theme);
            $theme->save();
        }
    }

    public function getActivePermittedThemes()
    {
        return Theme::where('enable', '=', true)
            ->where('in_school', '=', true)
            ->get();
    }
}
